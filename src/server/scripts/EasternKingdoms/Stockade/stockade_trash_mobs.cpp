

#include "ScriptPCH.h"


enum TrashSpells
{
	//Troble Maker 
	SPELL_DISRUPTINGSHOUT                   = 71022,
	SPELL_SABERLASH                         = 71021,

	//Captive
	SPELL_AMPLIFY_MAGIC_10N                 = 70408,
	SPELL_AMPLIFY_MAGIC_25N                 = 72336,
	SPELL_BLAST_WAVE_10N                    = 70407,
	SPELL_BLAST_WAVE_25N                    = 71151,
	SPELL_FIREBALL_10N                      = 70409,
	SPELL_FIREBALL_25N                      = 71153,
	SPELL_POLYMORPH_SPIDER                  = 70410,
	SPELL_SIPHON_ESSENCE                    = 70299,
};

enum TrashEvents
{
	//Troble Maker
	EVENT_DISRUPTINGSHOUT       = 8,
	EVENT_SABERLASH             = 9,

	//Captive
};


class npc_troblemaker : public CreatureScript
{
public:
	npc_troblemaker() : CreatureScript("npc_troblemaker") { }
	struct npc_troblemakerAI: public ScriptedAI
	{
		npc_troblemakerAI(Creature *c) : ScriptedAI(c)
		{
		}
		EventMap events;

		void Reset()
		{
			events.Reset();
		}

		void EnterCombat(Unit* who)
		{
			events.ScheduleEvent(EVENT_DISRUPTINGSHOUT, 8000);
			events.ScheduleEvent(EVENT_SABERLASH, 8000);
		}

		  void UpdateAI(uint32 diff)
		{
			//Return since we have no target
			if (!UpdateVictim())
				return;

			events.Update(diff);

			if (me->HasUnitState(UNIT_STATE_CASTING))
				return;

			while(uint32 eventId = events.ExecuteEvent())
			{
				switch(eventId)
				{
				case EVENT_DISRUPTINGSHOUT:
					DoCast(me, SPELL_DISRUPTINGSHOUT);
					me->MonsterYell("Wanna Try morder me!", LANG_UNIVERSAL, NULL);
					events.RescheduleEvent(EVENT_DISRUPTINGSHOUT, 8000);
					break;
				case EVENT_SABERLASH:
					DoCast(me->getVictim(), SPELL_SABERLASH, true);
					me->MonsterYell("you cannt do this before i do it", LANG_UNIVERSAL, NULL);
					events.RescheduleEvent(EVENT_SABERLASH, 8000);
					break;
				}
			}
			DoMeleeAttackIfReady();
		}
	};

	CreatureAI* GetAI(Creature* pCreature) const
	{
		return new npc_troblemakerAI(pCreature);
	}
};

class npc_captive : public CreatureScript
{
public:
	npc_captive() : CreatureScript("npc_captive") { }

	struct npc_captiveAI : public ScriptedAI
	{
		npc_captiveAI(Creature* pCreature) : ScriptedAI(pCreature) {}

		uint32 m_uiAMPLIFY_Timer;
		uint32 m_uiBLAST_Timer;
		uint32 m_uiFIREBALL_Timer;
		uint32 m_uiPOLYMORPH_Timer;

		void Reset()
		{
			m_uiAMPLIFY_Timer = urand(10000, 15000);
			m_uiBLAST_Timer = urand(8000, 10000);
			m_uiFIREBALL_Timer = 2000;
			m_uiPOLYMORPH_Timer = urand(9000, 12000);
		}

		void JustDied(Unit* killer)
		{
			me->MonsterYell("NO! Turmini I am sorry.", LANG_UNIVERSAL, NULL);
		}

		void UpdateAI(uint32 uiDiff)
		{
			if (!UpdateVictim())
				return;

			if (m_uiAMPLIFY_Timer <= uiDiff)
			{
				DoCast(me->getVictim(), RAID_MODE(SPELL_AMPLIFY_MAGIC_10N, SPELL_AMPLIFY_MAGIC_25N, SPELL_AMPLIFY_MAGIC_10N, SPELL_AMPLIFY_MAGIC_25N));
				m_uiAMPLIFY_Timer = urand(15000, 20000);
			}
			else
				m_uiAMPLIFY_Timer -= uiDiff;

			if (m_uiPOLYMORPH_Timer <= uiDiff)
			{
				if (Unit* target = SelectTarget(SELECT_TARGET_RANDOM, 0))
					DoCast(target, SPELL_POLYMORPH_SPIDER);

				m_uiPOLYMORPH_Timer = urand(15000, 18000);
			}
			else
				m_uiPOLYMORPH_Timer -= uiDiff;

			if (m_uiFIREBALL_Timer <= uiDiff)
			{
				DoCast(me->getVictim(), RAID_MODE(SPELL_FIREBALL_10N, SPELL_FIREBALL_25N, SPELL_FIREBALL_10N, SPELL_FIREBALL_25N));
				m_uiFIREBALL_Timer = urand(3000, 4000);
			}
			else
				m_uiFIREBALL_Timer -= uiDiff;

			if (m_uiBLAST_Timer <= uiDiff)
			{
				DoCastAOE(RAID_MODE(SPELL_BLAST_WAVE_10N, SPELL_BLAST_WAVE_25N, SPELL_BLAST_WAVE_10N, SPELL_BLAST_WAVE_25N));
				m_uiBLAST_Timer = urand(10000, 20000);
			}
			else
				m_uiBLAST_Timer -= uiDiff;

			DoMeleeAttackIfReady();   
		}
	};

	CreatureAI *GetAI(Creature *creature) const
	{
		return new npc_captiveAI(creature);
	}
};

void AddSC_stockade_trash()
{
	new npc_troblemaker();
	new npc_captive();
}