/********************************\
*    Umbra Gaming Inc (C) 2013   *
*      Erithol Boss Encounter    *
*        Type: Header File       *
*     By Nobody @ Umbra Gaming   *
\********************************/

#include "ScriptPCH.h"
using namespace std;

/* Player */
uint64 m_PlayerGUID;
string playerName;
/* Is Active */
bool isBattleActive = false;
bool hasLogged = false;
bool inZone = true;
/* Is Bosses Dead */
int isWaveBossDead = 0;

#define MSG_FIGHT_COMPUTER "I would like to fight!"

enum Events
{
	EVENT_NONE,
	EVENT_CHECK_ACTIVITY, // Check Activity of the battle : if true or false ? can start?
	EVENT_CHECK_WAVES, // Checks the waves so it can move on to the next event, if it is allowed
	EVENT_CHECK_PLAYER,
	EVENT_FIRST_WAVE,
	EVENT_FIRST_WAVE_TRASH,
	EVENT_FIRST_WAVE_ELITE,
	EVENT_SECOND_WAVE,
	EVENT_SECOND_WAVE_TRASH,
	EVENT_SECOND_WAVE_ELITE,
	EVENT_FINAL_WAVE_BOSS,
    EVENT_COMPLETED_WAVES,
};

enum bPhases
{
	/* Final Boss Phases */
	PHASE_START_COMBAT,
	PHASE_END,
};

enum SpellIds
{
	/* First Wave Spells */
	/* Demon Guards */
	SPELL_BERSERKER_CHARGE = 38907,
	SPELL_MORTAL_STRIKE = 68783,
	SPELL_DEEP_WOUNDS = 23256,
	/* First Wave Elite Spells */
	/* `- Spore Healer */
	SPELL_HEAL_REGROWTH = 48443, //heal+H.O.T.
	SPELL_HEAL_NORMAL = 48378, //heal normal
	SPELL_HEAL_REJUV = 48441, //H.O.T.
	SPELL_HEAL_AURA_HOTS = 38299, // Aura hots
	SPELL_FLASH_HEAL = 48071,
	SPELL_POWER_WORD_SHIELD = 20697, // Absorbs a ton of damage
	SPELL_HEAL_LIFEBLOOM = 67958,
	/* `- Xiombarg -> Tank Class */
	SPELL_BATTLE_STANCE = 2487, //warrior bstance
	SPELL_SHOUT_BUFF = 47436,	//attk pwr self buff
	SPELL_TANK_CHARGE = 11578,	//warrior charge
	SPELL_GROUND_STOMP = 47502, //thunderclap
	SPELL_EXECUTE = 47471,		//normal strike
	SPELL_TANK_STRIKE = 47450,	//normal strike
	SPELL_INCREASE_BLOCK_VALUE = 67515, // Increases block value
	SPELL_SHIELD_BLOCK = 69580, // Blocks attacks
	SPELL_INCREASE_DEFENSE_RATING = 60343, // Increases Defense by 180+
	SPELL_INCREASE_DODGE_RATING = 18062, // Increase dodge by 96
	SPELL_REND = 47465, // Rends the target ~Bleeds
	SPELL_SHIELD_BASH = 47488, // Bashes the target with a shield
	/* Second Wave Spells */
	SPELL_SORC_FIREBALL = 25306,
	SPELL_SORC_FLAMESTRIKE = 27086,
	SPELL_SORC_ICEBOLT = 28522, // Incase them in ice/stun
	SPELL_SORC_RAINOFFIRE = 31340,
	SPELL_SORC_BLIZZARD = 42940,
	/* Second Wave Trash Spells */
	SPELL_UNHOLY_BONE_SHIELD = 49222,
	SPELL_UNHOLY_AOE = 49938,
	SPELL_UNHOLY_PLAGUE_STRIKE = 49921,
	SPELL_UNHOLY_STANGULATE = 47476,
	SPELL_UNHOLY_DEATH_STRIKE = 49924,
	SPELL_UNHOLY_ARMY = 42650, // <- should happen with both NPC's simultaneously
	SPELL_ENRAGE = 59707,
	/* Second Wave Elite Spells */
	/*	`- Rider Brutes	*/
	SPELL_SHAMAN_LIGHTNING = 59159, // aoe
	SPELL_SHAMAN_CHAIN_LIGHTNING = 49269, // aoe
	SPELL_SHAMAN_MOONFIRE = 48463,
	SPELL_SHAMAN_STORM = 48467, // aoe
	/*	`- Rider Healer	*/
	SPELL_SHAMAN_STAM_BUFF = 48161, // fortitude buff
	SPELL_SHAMAN_SHIELD = 48066, // absorbs damage
	SPELL_SHAMAN_HEALING_BOLT = 66097,
	SPELL_SHAMAN_RENEW = 48068,
	SPELL_SHAMAN_GREATER_HEAL = 48063,
	/* Final Wave Boss Spells */
	SPELL_DRAGON_FIRE_SHIELD = 57108, //Reduces damage by 95% for 5s
	SPELL_DRAGON_ENGULF = 74562,
	SPELL_DRAGON_BREATH = 74525,
	SPELL_DRAGON_TAIL_WHIP = 74531,
	SPELL_DRAGON_BERSERK = 26662,
	SPELL_DRAGON_CLAW = 74524,
	SPELL_DRAGON_MASSIVE_FLAMESTRIKE = 26558,
	//SPELL_DRAGON_FIREBALL_BARRAGE = 37541,
	/* Others */
	SPELL_TELEPORT_VISUAL = 64446,
	
};

enum SpawnIds
{
	/* First Wave */
	NPC_DEMON_GUARD =             880010,
	NPC_DEMON_GUARD_MINI =        880020,
	NPC_DEMON_GUARD_MINI2 =       880030,
	NPC_DEMON_GUARD_MINI3 =       880040,
	/* First Wave Trash */
	NPC_WAVE_SPAWN_TRIGGER =      885000, 
	NPC_GARM_WATCHER       =      800500,
	NPC_FRIGID_PROTO_DRAKE =      800510,
	NPC_OSTERKILGR         =      800520,
	NPC_BLIGHT_FALCONER    =      800530,
	NPC_CONVERTED_HERO     =      800540,
	NPC_PORTAL =                  885010,
    /* First Wave Elite */ 					
	NPC_XIOMBARG_THE_TANK  =	  880110,	
	NPC_FIELD_MEDIC  =			  880120,	
	/* Second Wave */
	NPC_SORC_INITIATE = 		  880130, 
	NPC_SORC_INITIATE_1 =		  880140, 
	NPC_SORC_INITIATE_2 = 		  880150, 
	/* Second Wave Trash */
	NPC_THE_UNHOLY = 			  880160, 
	NPC_THE_UNHOLY_TWIN = 		  880170, 
	NPC_THE_UNHOLY_PET = 		  880180, 
	/* Second Wave Elite */
	NPC_SHAMAN_OFTHE_ICE =	 	  880190, 
	NPC_SHAMAN_OFTHE_ICE_1 =	  880200, 
	NPC_SHAMAN_OFTHE_ICE_2 = 	  880210, 
	/* Final Wave Boss */
	NPC_ERITHOL_BOSS = 			  255009, 
	NPC_ERITHOL_BOSS_TRIGGER =    880230,
	GOBJECT_ERITHOL_BOSS_RUNE =   183036,
};

enum eEnums
{
	ITEM_INTRAVENOUS_HEALING_POTION = 44698, // You can change the items they can heal with here
	KARAZHAN_ZONE = 3457,
	KARAZHAN_AREA = 3457, 
	PVP_END_TOKEN = 47241, // You can easily change this
	SOUND_HORN_WAVE_START = 7054,
	SOUND_WAVE_COMPLETE = 8571,
};

struct DragonMove
{
	uint32 gobject;
	uint32 spellId;
	float x, y, z, o;
};

static Position sTeleOut[] = 
{
	{ -10965.175f, -1956.912f, 79.973f, 1.409f } // Archway into Dining Area -> May remove this -> Not really needed
};

static DragonMove sMoveData[] =
{
	{GOBJECT_ERITHOL_BOSS_RUNE, SPELL_DRAGON_MASSIVE_FLAMESTRIKE, -10989.604f, -1911.566f, 78.868f, 4.583f } // Center of Banquet Hall
};

#define MAX_WAVE_SPAWN_LOCATIONS 20
const uint32 waveList[MAX_WAVE_SPAWN_LOCATIONS] =
{
	   /* First Wave Ids */
	   NPC_DEMON_GUARD,
	   /* First Wave Trash Ids */
	   NPC_WAVE_SPAWN_TRIGGER, NPC_GARM_WATCHER, NPC_FRIGID_PROTO_DRAKE, NPC_OSTERKILGR, NPC_BLIGHT_FALCONER, NPC_CONVERTED_HERO, NPC_PORTAL,
	   /* First Wave Elite Ids */
	   NPC_XIOMBARG_THE_TANK, NPC_FIELD_MEDIC,
	   /* Second Wave Ids */
	   NPC_SORC_INITIATE, NPC_SORC_INITIATE_1, NPC_SORC_INITIATE_2,
	   /* Second Wave Trash Ids */
	   NPC_THE_UNHOLY, NPC_THE_UNHOLY_TWIN, NPC_THE_UNHOLY_PET,
	   /* Second Wave Elite Ids */
	   NPC_SHAMAN_OFTHE_ICE, NPC_SHAMAN_OFTHE_ICE_1, NPC_SHAMAN_OFTHE_ICE_2,
	   /* Final Wave Boss */
	   NPC_ERITHOL_BOSS,
};

static Position m_WaveSpawns[] =
{
	/*    X               Y            Z           O      */
	             /* All Waves Spawns */
	{ -10982.952f, -1881.192f, 81.729f, 4.618f }, //Raised Platform
	             /* Trash Wave Spawns */
	{ -10951.407f, -1916.966f, 78.866f, 3.053f }, // Portal One Location
	{ -11019.447f, -1908.325f, 78.866f, 6.135f }  // Portal Two Location
	
};

void MessageOnWave(Creature * me, uint32 eventId)
{
	stringstream ss;
	switch(eventId)
	{
	   case EVENT_CHECK_ACTIVITY: // Before Wave 1 starts
		   ss << playerName.c_str()
			  << " get ready! Next wave begins in 10 seconds!";
		   break;

	   case EVENT_CHECK_WAVES:
		   {
			   if( isWaveBossDead == 1)
			   {
				   ss << "I can't believe it! "
					  << playerName.c_str()
					  << " and his party has killed the Demon Guards! We will see next wave, which is in 25 seconds, gear up!";
			   }
			   else if (isWaveBossDead == 2)
			   {
				   ss << "This is unreal! "
					  << playerName.c_str()
					  << " and his party has dominated the trash wave! Next wave is in 35 seconds! Get ready for a good experience.";
			   }
			   else if (isWaveBossDead == 3)
			   {	
				   ss << "Holy shit! "
					  << playerName.c_str()
					  << " and his party has demolished the elite! Good luck getting through the others... Second wave begins in 25 seconds! You may want to heal...";
			   }
			   else if (isWaveBossDead == 4)
			   {
				   ss << "Excellent! "
					  << playerName.c_str()
					  << " and his party has stricken down the Demon Sorcerers! Well done! Next wave is in 35 seconds, be prepared!";
			   }
			   else if (isWaveBossDead == 5)
			   {
				   ss << "Zing! I can't believe what I'm seeing! "
				      << playerName.c_str()
					  << " and his party has defeated the Unholy Twins, EPIC WIN! Next wave in 40 seconds, get ready!";
			   }
			   else if (isWaveBossDead == 6)
			   {
				   ss << "WOW! No Way! "
				      << playerName.c_str()
					  << " and his party has stopped the death riders dead in their tracks. Great job! Final wave beings in 35 seconds, get buffed up!";
			   }
			   else if (isWaveBossDead == 7)
			   {
				   ss << "Just...a moment of silence. "
				      << playerName.c_str()
					  << " and his party has just won this entire challenge! Congratulations!";
			   }
		   }break;

	   case EVENT_FIRST_WAVE:
		   ss << "First wave has begun!"
			  << " From the blood gates of hell, here is," 
			  << " Arena Master, Demon Guards!";
		   break;

	   case EVENT_FIRST_WAVE_TRASH:
		   ss << "First trash wave has begun!"
			  << " Bounding across the continent, you will face many challenges!";
		   break;

	   case EVENT_FIRST_WAVE_ELITE:
		   ss << "First elite wave has begun!"
			  << " Say your last words, this elite team wont give you another chance to."
			  << " Entering the arena, Hank the Tank and his heal happy spore companion. Prepare for the worst!";
		   break;
		  
	   case EVENT_SECOND_WAVE:
		   ss << "Second wave has begun!"
			  << " Trust your eyes and dont blink, this team of initiates only provide pain!"
			  << " The evil, the devestating.. The Demon sorcerers, maybe run?";
		   break;
	   
	   case EVENT_SECOND_WAVE_TRASH:
		  ss << "Second trash wave has begun!"
		     << " From the depths of the underworld, they were sent back here to take care of business!"
			 << " The deadliest duo, the Unholy Twins - Seal your fate, destroy them!";
		   break;
	   
	   case EVENT_SECOND_WAVE_ELITE:
		  ss << "Second elite wave has begun!"
		     << " The Riders of the Ice have entered the arena!"
			 << " Don't be fooled by their size, they will walk all over you. Attack!";
		   break;
		   
	   case EVENT_FINAL_WAVE_BOSS:
		  ss << "From deep into the Earth's core "
		     << " there lies a entirely different aspect of a challenge. "
			 << " Get Ready! Arena Dragon is here!";
		   break;
	}
	me->MonsterYell(ss.str().c_str(), LANG_UNIVERSAL, me->GetGUID());
}

void DoSendCompleteMessage(string who)
{
	stringstream ss;
	ss << who.c_str()
		<< "has completed the Erithol Battle Event!"
		<< "The event is now opened and ready for another victim!";
	sWorld->SendGlobalText(ss.str().c_str(), NULL);
}

void AddEndRewards(Player * player, uint32 honoramount, uint32 tokenId, uint32 tokenAmount)
{
	uint32 curHonor = player->GetHonorPoints();
	player->SetHonorPoints(curHonor + honoramount);
	ChatHandler(player->GetSession()).PSendSysMessage("Added %u honor!", honoramount);
	player->AddItem(tokenId, tokenAmount);
}
/* End Battle Functions */
void DoEndBattle(Creature * me)
{
	isBattleActive = false;
	m_PlayerGUID = NULL;
	playerName = "";
	me->DespawnOrUnsummon(1);
}

void DoEndBattle(Creature * me, SummonList summons)
{
	isBattleActive = false;
	m_PlayerGUID = NULL;
	playerName = "";
	summons.DespawnAll();
	me->DespawnOrUnsummon(1);
}
/* Increase Health Function ~ Boss Fight Pet */
void DoIncreaseHealth(Creature * me, float size)
{
	me->SetHealth(me->GetMaxHealth()+40000);
	me->SetFloatValue(OBJECT_FIELD_SCALE_X, size);
}
