/*-----------------------------------------
 * Title:                                 *
 * Buy Exp                                * 
 * CMD/NPC                                *
 * Scripted by: Easelm *                  * 
 * (C)Nomsoftware 'Nomsoft' 2011-2013     *
 *----------------------------------------*/
std::map<uint64, uint32> abuseList;
class cmd_spawn_exp_npc : public CommandScript
{
public:
cmd_spawn_exp_npc() : CommandScript("cmd_spawn_exp_npc") { }
ChatCommand* GetCommands() const
{
static ChatCommand commandTable[] =
{
{ "buyexp", SEC_GAMEMASTER, true, &HandleBuyExperienceCommand, "", NULL },
{ NULL, 0, false, NULL, "", NULL }
};
return commandTable;
};
static bool HandleBuyExperienceCommand(ChatHandler* handler, const char* args)
{
Player* player = handler->GetSession()->GetPlayer();
if (!abuseList.empty())
for (std::map<uint64, uint32>::const_iterator itr = abuseList.begin(); itr != abuseList.end(); ++itr)
if ((*itr).first == player->GetGUID())
if (GetMSTimeDiffToNow((*itr).second) < 180000) // 180000 = 3 minutes
{
ChatHandler(player->GetSession()).PSendSysMessage("You cannot spawn this npc for another %u minute(s)!", CalculateTimeInMinutes(GetMSTimeDiffToNow((*itr).second)));
return false;
}
else
abuseList.erase(player->GetGUID());
abuseList[player->GetGUID()] = getMSTime();
player->SummonCreature(/*NpcId*/80000, player->GetPositionX(), player->GetPositionY(), player->GetPositionZ(), player->GetOrientation(), TEMPSUMMON_TIMED_DESPAWN, 60000);
return true;
}
static uint32 CalculateTimeInMinutes(uint32 m_time)
{
uint32 howManyMinutes;
if (m_time >= 180000) // 180000 = 3 minutes
howManyMinutes = 3;
else if (m_time < 180000-60000)
howManyMinutes = 2;
else if (m_time > 180000-60000)
howManyMinutes = 1;
return howManyMinutes;
}
};
class npc_buy_exp : public CreatureScript
{
public:
npc_buy_exp() : CreatureScript("npc_buy_exp") { }
bool OnGossipHello(Player* player, Creature* creature)
{
player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "40000 XP [5g]", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+1);
player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Nevermind..", GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF+2);
player->SEND_GOSSIP_MENU(1, creature->GetGUID());
return true;
}
bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 actions)
{
player->PlayerTalkClass->ClearMenus();
switch (actions)
{
case GOSSIP_ACTION_INFO_DEF+1:
player->GiveXP(40000, player);
player->ModifyMoney(-50000);
break;
case GOSSIP_ACTION_INFO_DEF+2:
player->CLOSE_GOSSIP_MENU();
break;
}
return true;
}
};
void AddSC_buy_experience()
{
new cmd_spawn_exp_npc();
new npc_buy_exp();
}