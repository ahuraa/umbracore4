#include "ScriptPCH.h"
#define TOKEN_ID   160021
 
class Level_NPC_21 : public CreatureScript
{
public:
Level_NPC_21() : CreatureScript("Level_NPC_21") {}
bool OnGossipHello(Player* pPlayer, Creature* _creature)
{
  pPlayer->ADD_GOSSIP_ITEM(7, "Greetings Champion. I can make you level 210.", GOSSIP_SENDER_MAIN, 111212);
  pPlayer->ADD_GOSSIP_ITEM(10, "Set My Level to 210 (Requires L210 Token)", GOSSIP_SENDER_MAIN, 1);
  
				pPlayer->PlayerTalkClass->SendGossipMenu(907, _creature->GetGUID());
                return true;
}
bool OnGossipSelect(Player* pPlayer, Creature* _creature, uint32 uiSender, uint32 uiAction)
{
		pPlayer->PlayerTalkClass->ClearMenus();
		if(uiAction != 0)
			if (pPlayer->HasItemCount(TOKEN_ID, uiAction, true))
			{
				pPlayer->GiveLevel(uiAction*210); 
				pPlayer->DestroyItemCount(TOKEN_ID, uiAction, true);
				pPlayer->GetSession()->SendAreaTriggerMessage("You are now Level %u!", uiAction*210);
				pPlayer->PlayerTalkClass->SendCloseGossip();
				return true;
			}
			else
				pPlayer->GetSession()->SendNotification("You don't have the required token");
		OnGossipHello(pPlayer, _creature);
		return true;
	}
};
void AddSC_Level_NPC_21()
{
	new Level_NPC_21();
}