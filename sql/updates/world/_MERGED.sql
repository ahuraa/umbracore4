-- -------------------------------------------------------- 
-- 2013_04_20_00_world_creature_loot_template.sql 
-- -------------------------------------------------------- 
-- Insert Frozen Orb into The Prophet Tharon'ja's loot template
DELETE FROM `creature_loot_template` WHERE `entry`=31360 and `item`=43102;
INSERT INTO `creature_loot_template` (`entry`,`item`,`ChanceOrQuestChance`,`lootmode`,`groupid`,`mincountOrRef`,`maxcount`) VALUES
(31360,43102,100,1,0,1,1);
 
 
-- -------------------------------------------------------- 
-- 2013_04_20_01_world_conditions.sql 
-- -------------------------------------------------------- 
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=23 AND `SourceGroup`=12944 AND `SourceEntry` IN (20761,17051,17022,17018,17023,19444);
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=23 AND `SourceGroup`=12944 AND `SourceEntry` IN (19219,17017,17025,19330,19448,17059,17060,17049,19206);
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=23 AND `SourceGroup`=12944 AND `SourceEntry` IN (19220,19332,19333,19331,19449,19209,19208,19207,17053,17052);
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`=23 AND `SourceGroup`=12944 AND `SourceEntry` IN (19211,19210,19212,20040);
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
-- Conditon rep Friendly
(23,12944,20761,0,0,5,0,59,16|32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Friendly with Thorium Brotherhood'),
(23,12944,17051,0,0,5,0,59,16|32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Friendly with Thorium Brotherhood'),
(23,12944,17022,0,0,5,0,59,16|32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Friendly with Thorium Brotherhood'),
(23,12944,17018,0,0,5,0,59,16|32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Friendly with Thorium Brotherhood'),
(23,12944,17023,0,0,5,0,59,16|32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Friendly with Thorium Brotherhood'),
(23,12944,19444,0,0,5,0,59,16|32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Friendly with Thorium Brotherhood'),
-- Conditon rep Honored
(23,12944,19219,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
(23,12944,17017,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
(23,12944,17025,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
(23,12944,19330,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
(23,12944,19448,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
(23,12944,17059,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
(23,12944,17060,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
(23,12944,17049,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
(23,12944,19206,0,0,5,0,59,32|64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Honored with Thorium Brotherhood'),
-- Conditon rep Revered
(23,12944,19220,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,19332,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,19333,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,19331,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,19449,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,19209,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,19208,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,19207,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,17053,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
(23,12944,17052,0,0,5,0,59,64|128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Revered with Thorium Brotherhood'),
-- Conditon rep Exalted
(23,12944,19211,0,0,5,0,59,128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Exalted with Thorium Brotherhood'),
(23,12944,19210,0,0,5,0,59,128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Exalted with Thorium Brotherhood'),
(23,12944,19212,0,0,5,0,59,128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Exalted with Thorium Brotherhood'),
(23,12944,20040,0,0,5,0,59,128,0,0,0, '', 'Lokhtos Darkbargainer - Only sell item if Exalted with Thorium Brotherhood');
 
 
-- -------------------------------------------------------- 
-- 2013_04_21_00_world_gameobject_loot_template.sql 
-- -------------------------------------------------------- 
-- Updating maxcount for Scarab Scarab Bag
UPDATE `item_loot_template` SET `maxcount`=5 WHERE `entry`=21156 AND `item` IN (20858,20859,20860,20861,20862,20863,20864,20865);
UPDATE `reference_loot_template` SET `maxcount`=2 WHERE `entry`=14001 AND `item` IN (20866,20867,20868,20869,20870,20871,20872,20873);
-- Updating loot for Scarab Coffer
DELETE FROM `gameobject_loot_template` WHERE `entry` IN (17532,17533);
INSERT INTO `gameobject_loot_template` VALUES 
-- Scarab Coffer
(17532,21156,-100,1,0,1,1), -- Always drop a Sacrab Bag
(17532,1,100,1,0,-14001,1), -- Drop a AQ20 idol from reference
-- Large Scarab Coffer
(17533,21156,-100,1,0,1,1), -- Always drop a Sacrab Bag
(17533,1,100,1,0,-14502,1); -- Drop a AQ40 idol from reference
-- Updating loot for Large Scarab Coffer
DELETE FROM `reference_loot_template` WHERE `entry`=14502;
INSERT INTO `reference_loot_template` VALUES 
(14502,20877,0,1,1,1,3), -- 1 to 3  Idol of the Sage
(14502,20879,0,1,1,1,3), -- 1 to 3  Idol of Life
(14502,20878,0,1,1,1,3), -- 1 to 3  Idol of Rebirth
(14502,20881,0,1,1,1,3), -- 1 to 3  Idol of Strife
(14502,20874,0,1,1,1,3), -- 1 to 3  Idol of the Sun
(14502,20882,0,1,1,1,3), -- 1 to 3  Idol of War
(14502,20876,0,1,1,1,3), -- 1 to 3  Idol of Death
(14502,20875,0,1,1,1,3); -- 1 to 3  Idol of Night
 
 
-- -------------------------------------------------------- 
-- 2013_04_21_00_world_skinning_loot_template.sql 
-- -------------------------------------------------------- 
-- Simplify skinloot for Axtroz and make sure something drops as well
UPDATE `skinning_loot_template` SET `ChanceOrQuestChance`=50 WHERE `entry`=12899 AND `item`=8170;
UPDATE `skinning_loot_template` SET `ChanceOrQuestChance`=35 WHERE `entry`=12899 AND `item`=15414;
UPDATE `skinning_loot_template` SET `ChanceOrQuestChance`=10 WHERE `entry`=12899 AND `item`=8165;
UPDATE `skinning_loot_template` SET `ChanceOrQuestChance`=0 WHERE `entry`=12899 AND `item`=4304;
-- Update dropchance for rare items on Axtroz
UPDATE `creature_loot_template` SET `ChanceOrQuestChance`=5 WHERE `entry`=12899 AND `mincountOrRef`=-24020;
 
 
-- -------------------------------------------------------- 
-- 2013_04_21_01_world_gameobject_template.sql 
-- -------------------------------------------------------- 
DELETE FROM `gameobject_template` WHERE `entry`=202193;
INSERT INTO `gameobject_template` (`entry`, `type`, `displayId`, `name`, `IconName`, `castBarCaption`, `unk1`, `faction`, `flags`, `size`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `data0`, `data1`, `data2`, `data3`, `data4`, `data5`, `data6`, `data7`, `data8`, `data9`, `data10`, `data11`, `data12`, `data13`, `data14`, `data15`, `data16`, `data17`, `data18`, `data19`, `data20`, `data21`, `data22`, `data23`, `AIName`, `ScriptName`, `WDBVerified`) VALUES 
(202193, 5, 9050, 'Silver Covenant Banner', '', '', '', 0, 0, 0.8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 15595);
 
 
-- -------------------------------------------------------- 
-- 2013_04_21_02_world_sai.sql 
-- -------------------------------------------------------- 
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=17210 AND `source_type`=0 AND `id`=2 AND `link`=3;
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=23619 AND `source_type`=0 AND `id`=1 AND `link`=2;
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=23623 AND `source_type`=0 AND `id`=1 AND `link`=2;
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=23624 AND `source_type`=0 AND `id`=1 AND `link`=2;
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=23625 AND `source_type`=0 AND `id`=1 AND `link`=2;
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=23626 AND `source_type`=0 AND `id`=1 AND `link`=2;
UPDATE `smart_scripts` SET `link`=0, `event_type`=0 WHERE  `entryorguid`=83600 AND `source_type`=9 AND `id`=0 AND `link`=1;
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=1173 AND `source_type`=0 AND `id`=8 AND `link`=9;
UPDATE `smart_scripts` SET `link`=0 WHERE  `entryorguid`=23339 AND `source_type`=0 AND `id`=8 AND `link`=9;
UPDATE `creature_template` SET `ScriptName`='' WHERE  `entry` IN (10981,10982,10986,10987,10990,11602,11603,11605,11678,11839,12050,12051,12053,12127,13078,13080,13089,13097,13098,13179,13284,13316,13317,13326,13328,13396,13438,13442,13443,13447,13577,13797,13798,14283);
 
 
-- -------------------------------------------------------- 
-- 2013_04_22_00_world_misc.sql 
-- -------------------------------------------------------- 
DROP TABLE IF EXISTS ip2nation;
DROP TABLE IF EXISTS ip2nationCountries;

DELETE FROM `command` WHERE `name` in ('account lock', 'account lock ip', 'account lock country');
INSERT INTO `command` (`name`,`security`,`help`) VALUES
('account lock ip', 0, 'Syntax: .account lock ip [on|off]\nAllow login from account only from current used IP or remove this requirement.'),
('account lock country', 0, 'Syntax: .account lock country [on|off]\nAllow login from account only from current used Country or remove this requirement.');
 
 
-- -------------------------------------------------------- 
-- _MERGED.sql 
-- -------------------------------------------------------- 
 
 
