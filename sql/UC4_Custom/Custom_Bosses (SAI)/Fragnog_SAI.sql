--The Council of Three
--RageFire Chasm
--Boss Fragnog's SAI SQL
--UmbraCore2 (C) 2013
--Nobody

DELETE FROM `smart_scripts` WHERE (`entryorguid`=800052 AND `source_type`=0);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES 
(800052, 0, 0, 0, 2, 0, 100, 0, 90, 100, 6000, 7000, 11, 116, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, "FROSTBOLT"),
(800052, 0, 2, 0, 2, 0, 100, 0, 70, 80, 10000, 13000, 11, 134, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "FIRE SHELD"),
(800052, 0, 3, 0, 2, 0, 100, 0, 60, 70, 0, 0, 11, 8269, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, "FRENZY"),
(800052, 0, 4, 0, 2, 0, 100, 0, 50, 60, 6000, 7000, 11, 133, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, "FIREBALL"),
(800052, 0, 10, 0, 2, 0, 100, 1, 1, 10, 0, 0, 11, 68335, 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, "Enrage at 10%"),
(800052, 0, 12, 0, 2, 0, 100, 1, 30, 40, 8000, 9000, 11, 2565, 2, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, "Shield Block"),
(800052, 0, 14, 0, 4, 0, 100, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, "Yell 1"),
(800052, 0, 15, 0, 2, 0, 100, 1, 40, 50, 0, 0, 1, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, "Yell 2"),
(800052, 0, 16, 0, 2, 0, 100, 1, 20, 30, 0, 0, 1, 3, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, "Yell 3"),
(800052, 0, 17, 0, 2, 0, 100, 1, 1, 5, 0, 0, 1, 4, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, "Yell 4");